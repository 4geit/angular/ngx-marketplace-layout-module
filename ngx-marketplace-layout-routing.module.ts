import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxMarketplaceLayoutComponent } from './ngx-marketplace-layout.component';
import { NgxMarketplaceHomeComponent } from '@4geit/ngx-marketplace-home-component';
import { NgxMarketplaceProductDetailComponent } from '@4geit/ngx-marketplace-product-detail-component';
import { NgxCheckoutComponent } from '@4geit/ngx-checkout-component';
import { NgxMarketplaceAccountComponent } from '@4geit/ngx-marketplace-account-component';
import { NgxMarketplaceCategoryComponent } from '@4geit/ngx-marketplace-category-component';
import { NgxLoginComponent } from '@4geit/ngx-login-component';

const routes: Routes = [{
  path: '',
  component: NgxMarketplaceLayoutComponent,
  children: [
    { path: '', component: NgxMarketplaceHomeComponent },
    { path: 'category/:slug', component: NgxMarketplaceCategoryComponent },
    { path: 'product/:slug', component: NgxMarketplaceProductDetailComponent },
    { path: 'checkout', component: NgxCheckoutComponent },
    { path: 'account', component: NgxMarketplaceAccountComponent },
    { path: 'login', component: NgxLoginComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgxMarketplaceLayoutRoutingModule { }
