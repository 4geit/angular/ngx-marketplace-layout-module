import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMarketplaceLayoutRoutingModule } from './ngx-marketplace-layout-routing.module';
import { NgxMarketplaceLayoutComponent } from './ngx-marketplace-layout.component';

import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxMarketplaceHeaderModule } from '@4geit/ngx-marketplace-header-component';
import { NgxFooterModule } from '@4geit/ngx-footer-component';
import { NgxMarketplaceHomeModule } from '@4geit/ngx-marketplace-home-component';
import { NgxMarketplaceProductDetailModule } from '@4geit/ngx-marketplace-product-detail-component';
import { NgxCheckoutModule } from '@4geit/ngx-checkout-component';
import { NgxMarketplaceAccountModule } from '@4geit/ngx-marketplace-account-component';
import { NgxMarketplaceCategoryModule } from '@4geit/ngx-marketplace-category-component';
import { NgxLoginModule } from '@4geit/ngx-login-component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    NgxMarketplaceHeaderModule,
    NgxFooterModule,
    NgxMarketplaceHomeModule,
    NgxMarketplaceProductDetailModule,
    NgxMarketplaceLayoutRoutingModule,
    NgxCheckoutModule,
    NgxMarketplaceAccountModule,
    NgxMarketplaceCategoryModule,
    NgxLoginModule,
  ],
  declarations: [
    NgxMarketplaceLayoutComponent
  ]
})
export class NgxMarketplaceLayoutModule { }
