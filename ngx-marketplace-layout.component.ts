import { Component } from '@angular/core';

@Component({
  selector: 'ngx-marketplace-layout',
  template: require('pug-loader!./ngx-marketplace-layout.component.pug')(),
  styleUrls: ['./ngx-marketplace-layout.component.scss']
})
export class NgxMarketplaceLayoutComponent {
}
