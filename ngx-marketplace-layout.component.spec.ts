import { TestBed, async } from '@angular/core/testing';

import { NgxMarketplaceLayoutComponent } from './ngx-marketplace-layout.component';

describe('NgxMarketplaceLayoutComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NgxMarketplaceLayoutComponent
      ],
    }).compileComponents();
  }));

  it('should create the account', async(() => {
    const fixture = TestBed.createComponent(NgxMarketplaceLayoutComponent);
    const account = fixture.debugElement.componentInstance;
    expect(account).toBeTruthy();
  }));

  it(`should have as title 'account works!'`, async(() => {
    const fixture = TestBed.createComponent(NgxMarketplaceLayoutComponent);
    const account = fixture.debugElement.componentInstance;
    expect(account.title).toEqual('account works!');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(NgxMarketplaceLayoutComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('account works!');
  }));
});
