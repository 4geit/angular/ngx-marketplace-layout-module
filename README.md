# @4geit/ngx-marketplace-layout-module [![npm version](//badge.fury.io/js/@4geit%2Fngx-marketplace-layout-module.svg)](//badge.fury.io/js/@4geit%2Fngx-marketplace-layout-module)

---

module to setup a marketplace layout

## Installation

1. A recommended way to install ***@4geit/ngx-marketplace-layout-module*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-marketplace-layout-module) package manager using the following command:

```bash
npm i @4geit/ngx-marketplace-layout-module --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-marketplace-layout-module
```

2. You need to import the `NgxMarketplaceLayoutModule` class in whatever module you want in your project for instance `app.module.ts` as follows:

```js
import { NgxMarketplaceLayoutModule } from '@4geit/ngx-marketplace-layout-module';
```

And you also need to add the `NgxMarketplaceLayoutModule` module within the `@NgModule` decorator as part of the `imports` list:

```js
@NgModule({
  // ...
  imports: [
    // ...
    NgxMarketplaceLayoutModule,
    // ...
  ],
  // ...
})
export class AppModule { }
```
